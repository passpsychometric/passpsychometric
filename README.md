PassPsychometric (TakeMyTests): Is the leading provider of Psychometric Test support. Pass your psychometric tests today. 

We specialise in providing expert assistance for all types of psychometric tests. Simply order what you need and we'll do the rest.

We can help with numerical reasoning, verbal reasoning, situational judgement assessments and many others.

Website : https://takemytests.com
